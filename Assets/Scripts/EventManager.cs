﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzlePlatformer
{
    public static class EventManager 
    {
        //Gameplay delegates
        public delegate void SpawnObject(string name);
        public delegate void GrabObject(string name);
        public delegate void DoorControl(string name);

        //UI delegates
        //public delegate void ShowSpawnInstruction(bool state, bool show);
        //public delegate void ShowGrabInstruction(bool state, bool objectGrabbed);
        public delegate void ShowUI(GameObject gameObject, bool state);

        //Gameplay events
        public static event SpawnObject ObjectsSpawn;
        public static event GrabObject ObjectsGrab;
        public static event DoorControl ControlDoors;

        //UI events
        //public static event ShowSpawnInstruction InstructionSpawn;
        //public static event ShowGrabInstruction InstructionGrab;
        public static event ShowUI UserInterface;


        public static void ObjectSpawn(string name)
        {
            ObjectsSpawn(name);
        }

        public static void ObjectGrab(string name)
        {
            ObjectsGrab(name);
        }

        public static void DoorsControl(string name)
        {
            ControlDoors(name);
        }

        //public static void SpawnInstruction(bool state, bool show)
        //{
        //    InstructionSpawn(state, show);
        //}

        //public static void GrabInstruction(bool state, bool objectGrabbed)
        //{
        //    InstructionGrab(state, objectGrabbed);
        //}

        public static void UIControl(GameObject gameObject, bool state)
        {
            UserInterface(gameObject, state);
        }
    }
}
