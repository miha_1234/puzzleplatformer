﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public delegate void InteractionDelegate();
namespace PuzzlePlatformer
{

    public class PlayerController : MonoBehaviour
    {
        [Range(0, 10)]
        public float Speed = 3;
        [Range(0, 50)]
        public float MaxSpeed = 0;
        public int Multiplyer = 2;
        public Vector3 Offset;
        public Vector3 OffsetGrab;
        public bool ObjectGrabbed;
        public GameObject GrabbedObject;

        private Transform _transform;
        [SerializeField]
        private Rigidbody _rigidbody;
        [SerializeField]
        private float _jumpPower = 5f;
        [SerializeField]
        private bool _isGrounded = false;
        private Camera _camera;
        [SerializeField]
        private float _maxInteractDistance = 5.0f;
        private Transform _objectParent;
        private Quaternion GrabbedObjectRotation = Quaternion.identity;
        [SerializeField]
        private float _rotateStep = 5f;

        // Start is called before the first frame update
        void Start()
        {
            _transform = transform;
            _camera = Camera.main;
            _rigidbody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            _transform.eulerAngles = new Vector3(0, _camera.transform.rotation.eulerAngles.y, 0);
            _camera.transform.position = _transform.position + Offset;

            if (ObjectGrabbed && GrabbedObject != null)
            {
                GrabbedObject.transform.position = _camera.ViewportToWorldPoint(OffsetGrab);
                GrabbedObject.transform.up = Vector3.up;
                GrabbedObject.transform.localRotation = GrabbedObjectRotation;

                if (Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    if (Input.GetAxis("Mouse ScrollWheel") < 0)
                    {
                        RotateGrabbedObject(_rotateStep);
                    }
                    if (Input.GetAxis("Mouse ScrollWheel") > 0)
                    {
                        RotateGrabbedObject(-_rotateStep);
                    }

                }
            }

        }

        private void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.W) && CanMove(_rigidbody, MaxSpeed))
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    _rigidbody.velocity += _transform.forward * Speed * Multiplyer;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    _rigidbody.velocity += -_transform.right * Speed;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    _rigidbody.velocity += _transform.right * Speed;
                }

                _rigidbody.velocity += _transform.forward * Speed;
            }

            else if (Input.GetKey(KeyCode.S) && CanMove(_rigidbody, MaxSpeed))
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    _rigidbody.velocity -= _transform.forward * Speed * Multiplyer;
                }

                else if (Input.GetKey(KeyCode.A))
                {
                    _rigidbody.velocity += -_transform.right * Speed;
                }

                else if (Input.GetKey(KeyCode.D))
                {
                    _rigidbody.velocity += _transform.right * Speed;
                }

                _rigidbody.velocity += -_transform.forward * Speed;
            }

            if (Input.GetKey(KeyCode.A) && CanMove(_rigidbody, MaxSpeed))
            {
                if (Input.GetKey(KeyCode.W) && CanMove(_rigidbody, MaxSpeed))
                {
                    _rigidbody.velocity += -_transform.right * Speed;
                }

                else if (Input.GetKey(KeyCode.S))
                {
                    _rigidbody.velocity -= -_transform.forward * Speed;
                }

                _rigidbody.velocity += -_transform.right * Speed;
            }

            else if (Input.GetKey(KeyCode.D) && CanMove(_rigidbody, MaxSpeed))
            {
                if (Input.GetKey(KeyCode.W))
                {
                    _rigidbody.velocity += _transform.forward * Speed;
                }

                if (Input.GetKey(KeyCode.S))
                {
                    _rigidbody.velocity -= -_transform.forward * Speed;
                }

                _rigidbody.velocity += _transform.right * Speed;
            }

            if (Input.GetKeyDown(KeyCode.Space) && CanJump(_rigidbody, _isGrounded))
            {
                _rigidbody.velocity += _transform.up * _jumpPower;
            }

            if (Input.GetKeyDown(KeyCode.F) && ObjectGrabbed)
            {
                ReleaseObject(GrabbedObject.transform.parent.GetChild(GrabbedObject.transform.parent.childCount - 1).gameObject);
            }

            if (Input.GetKey(KeyCode.None))
            {
                _rigidbody.velocity += Vector3.zero;
            }
        }

        private void LateUpdate()
        {
            int layerMask = 1 << 8;
            layerMask = ~layerMask;

            RaycastHit hit;

            if (Physics.Raycast(_camera.transform.position, _camera.transform.TransformDirection(Vector3.forward), out hit, _maxInteractDistance, layerMask))
            {
                Debug.DrawRay(_camera.transform.position, _camera.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

                if (hit.transform.gameObject.GetComponent<Rigidbody>() != null)
                {
                    if (Input.GetKeyUp(KeyCode.F) && !ObjectGrabbed)
                    {
                        GrabObject(hit.transform.gameObject);
                        ObjectGrabbed = true;
                    }
                }

                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (hit.transform.GetComponent<ObjectSpawnerButton>())
                    {
                        EventManager.ObjectSpawn(hit.transform.name);
                    }

                    if (hit.transform.GetComponent<DoorController>())
                    {
                        EventManager.DoorsControl(hit.transform.name);
                    }
                }

                EventManager.UIControl(hit.transform.gameObject, ObjectGrabbed);
            }
        }

        private void GrabObject(GameObject objectToGrab)
        {
            _objectParent = objectToGrab.transform.parent;
            objectToGrab.transform.parent = _transform;
            Rigidbody _rb = objectToGrab.GetComponent<Rigidbody>();
            _rb.useGravity = false;
            _rb.isKinematic = true;
            _rb.freezeRotation = true;
            Physics.IgnoreCollision(objectToGrab.GetComponent<Collider>(), GetComponent<Collider>(), true);
            GrabbedObject = objectToGrab;
        }

        private void RotateGrabbedObject(float step)
        {
            GrabbedObject.transform.localEulerAngles = new Vector3(GrabbedObject.transform.localEulerAngles.x,
                                                    GrabbedObject.transform.localEulerAngles.y + step,
                                                    GrabbedObject.transform.localEulerAngles.z);
            GrabbedObjectRotation = GrabbedObject.transform.localRotation;
        }

        private void ReleaseObject(GameObject objectToRelease)
        {
            Rigidbody _rb = objectToRelease.GetComponent<Rigidbody>();
            objectToRelease.transform.parent = _objectParent;
            _rb.useGravity = true;
            _rb.isKinematic = false;
            _rb.freezeRotation = false;
            Physics.IgnoreCollision(objectToRelease.GetComponent<Collider>(), GetComponent<Collider>(), false);
            ObjectGrabbed = false;
        }

        private bool CanMove(Rigidbody rigidbody, float maxSpeed)
        {
            if (Mathf.Abs(rigidbody.velocity.x) <= maxSpeed && Mathf.Abs(rigidbody.velocity.z) <= maxSpeed)
            {
                return true;
            }
            else
                return false;
        }

        private bool CanJump(Rigidbody rigidbody, bool isGrounded)
        {
            if (isGrounded || Mathf.Approximately(rigidbody.velocity.y, 0))
            {
                return true;
            }
            else
                return false;
        }

        private void OnTriggerEnter(Collider ground)
        {
            //ground condition TODO
            _isGrounded = true;
        }

        private void OnTriggerExit(Collider ground)
        {
            _isGrounded = false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawCube(Camera.main.ViewportToWorldPoint(OffsetGrab), new Vector3(1, 1, 1));
        }
    }
}