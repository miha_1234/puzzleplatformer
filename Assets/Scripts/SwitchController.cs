﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzlePlatformer
{

    public class SwitchController : MonoBehaviour
    {
        public bool ShowGizmos = false;
        public bool TargetObject = false;
        public bool AutoChangeState = true;
        public bool AutoAction = false;
        [SerializeField]
        private bool _anyKey = false;

        private LayerMask _lastMask = 0;
        private string _userName;

        public Vector3 SwitchSize;
        public Vector3 SwitchPositionOffset;

        public bool IsPressed(LayerMask mask, string name)
        {
            _userName = name;
            _lastMask = mask;
            Collider[] hitColliders = Physics.OverlapBox(transform.position + SwitchPositionOffset, SwitchSize, Quaternion.identity);
            if (AutoChangeState)
            {
                TargetObject = false;
            }

            foreach (Collider hit in hitColliders)
            {
                if ((hit.gameObject.layer == mask || _anyKey) && (this.gameObject.layer != hit.gameObject.layer) && (hit.gameObject.layer != 10))
                {
                    TargetObject = true;
                    break;
                }
                else
                    TargetObject = false;
            }

            return TargetObject;
        }

        private void OnDrawGizmos()
        {
            Vector3 switchSize = SwitchSize;
            if (ShowGizmos)
            {
                Color color = GetComponent<Renderer>().sharedMaterial.color;
                color = new Color(color.r, color.g, color.b, color.a / 2);
                Gizmos.color = color;
                Gizmos.DrawCube(transform.position + SwitchPositionOffset, switchSize * 2);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (_lastMask > 11 && AutoChangeState)
            {
                IsPressed(_lastMask, _userName);
            }

            if (_lastMask > 11 && AutoAction)
            {
                EventManager.DoorsControl(_userName);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (_lastMask > 11 && AutoChangeState)
            {
                IsPressed(_lastMask, _userName);
            }

            if (_lastMask > 11 && AutoAction)
            {
                EventManager.DoorsControl(_userName);
            }
        }
    }
}
