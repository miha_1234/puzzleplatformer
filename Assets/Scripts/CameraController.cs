﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzlePlatformer
{

    public class CameraController : MonoBehaviour
    {

        public float SpeedH = 2.0f;
        public float SpeedV = 2.0f;

        [SerializeField]
        private float _yaw = 0f;
        [SerializeField]
        private float _pitch = 0f;
        public float MaxPitch = 30f;
        public float MinPitch = -20f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            _yaw += SpeedH * Input.GetAxis("Mouse X");
            _pitch -= SpeedV * Input.GetAxis("Mouse Y");

            if (_pitch < MinPitch)
            {
                _pitch = MinPitch;
            }
            else if (_pitch > MaxPitch)
            {
                _pitch = MaxPitch;
            }

            transform.eulerAngles = new Vector3(_pitch, _yaw, 0.0f);
        }
    }
}