﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzlePlatformer
{

    public class DoorController : MonoBehaviour
    {
        public bool ShowGizmos = false;
        public List<SwitchController> DoorSwitch;
        public GameObject DoorKey;
        public bool DoorOpened;
        public bool DoorLocked
        {
            get
            {
                bool state = true;
                foreach (SwitchController doorSwitch in DoorSwitch)
                {
                    state = state && CanChangePosition(doorSwitch);
                }
                return state;
            }
            set { }
        }

        [SerializeField]
        private bool _isCoroutineRunning = false;
        private Transform _transform;
        private Vector3 _startPosition;
        private Vector3 _endPosition;
        [SerializeField]
        private Vector3 _endOffset;
        [SerializeField]
        private float _openTime;

        private void Awake()
        {
            _transform = transform;
            _startPosition = _transform.position;
            _endPosition = _startPosition + _endOffset;
            EventManager.ControlDoors += ChangePosition;
            foreach (SwitchController doorSwitch in DoorSwitch)
            {
                DoorLocked = CanChangePosition(doorSwitch);
            }
        }

        private void ChangePosition(string name)
        {
            if (_isCoroutineRunning)
            {
                StopCoroutine(ChangeDoorPosition());
                _isCoroutineRunning = false;
            }
                
            if (name == this.name && DoorLocked && !_isCoroutineRunning)
            {
                StartCoroutine(ChangeDoorPosition());
            }
        }

        private IEnumerator ChangeDoorPosition()
        {
            _isCoroutineRunning = true;
            float startTime = Time.time;
            float journeyLength = Vector3.Distance(_startPosition, _endPosition);
            float fractionOfJourney = 0;

            if (_transform.position == _startPosition)
            {
                DoorOpened = true;
                while (fractionOfJourney < 1)
                {
                    float distanceCovered = (Time.time - startTime) * _openTime;
                    fractionOfJourney = distanceCovered / journeyLength;
                    _transform.position = Vector3.Lerp(_startPosition, _endPosition, fractionOfJourney);

                    yield return new WaitForEndOfFrame();
                }
            }

            else if (_transform.position == _endPosition)
            {
                DoorOpened = false;
                while (fractionOfJourney < 1)
                {
                    float distanceCovered = (Time.time - startTime) * _openTime;
                    fractionOfJourney = distanceCovered / journeyLength;
                    _transform.position = Vector3.Lerp(_endPosition, _startPosition, fractionOfJourney);

                    yield return new WaitForEndOfFrame();
                }
            }

            else
            {
                yield return null;
            }

            _isCoroutineRunning = false;
        }

        private bool CanChangePosition(SwitchController switchController)
        {
            return switchController != null ? switchController.IsPressed(DoorKey.layer, this.name) : true;
        }

        private void OnDrawGizmos()
        {
            if (ShowGizmos)
            {
                Gizmos.color = GetComponent<Renderer>().sharedMaterial.color;
                Color color = Gizmos.color;
                color.a /= 2;
                Gizmos.color = color;
                Gizmos.DrawCube(transform.position + _endOffset, transform.localScale);
                //Gizmos.DrawMesh(transform.gameObject.GetComponent<Mesh>(), transform.position + _endOffset, transform.rotation, transform.localScale);
            }
        }
    }
}
