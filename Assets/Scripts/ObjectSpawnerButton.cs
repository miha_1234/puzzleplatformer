﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public delegate void ObjectSpawn();
namespace PuzzlePlatformer
{
    public class ObjectSpawnerButton : MonoBehaviour
    {
        public bool ShowGizmos = false;
        public SwitchController Switch;
        public GameObject PrefabToSpawn;
        public Transform ObjectParent;
        [SerializeField]
        private Vector3 SpawnLocationOffset = Vector3.one;
        [SerializeField]
        [Range(0, 20)]
        public List<GameObject> SpawnedObjects = new List<GameObject>();
        [SerializeField]
        [Range(1, 20)]
        private int _maxObjects = 5;
        [Range(0, 50)]
        public float FadeTime = 5;

        [Range(0, 1)]
        public float Alpha = 1f;

        private void Awake()
        {
            EventManager.ObjectsSpawn += SpawnObject;
        }

        private void SpawnObject(string name)
        {
            if (name == this.name && CanSpawn(Switch))
            {
                if (SpawnedObjects.Count >= _maxObjects)
                {
                    GameObject cloneToRemove = SpawnedObjects[0];
                    SpawnedObjects.RemoveAt(0);
                    if (cloneToRemove == FindObjectOfType<PlayerController>().GrabbedObject)
                    {
                        FindObjectOfType<PlayerController>().ObjectGrabbed = false;
                    }
                    StartCoroutine(DestoyDelay(cloneToRemove));

                }

                GameObject cloneObject = Instantiate(PrefabToSpawn, transform.position + SpawnLocationOffset, Quaternion.identity, ObjectParent);
                SpawnedObjects.Insert(SpawnedObjects.Count, cloneObject);
                StartCoroutine(FadeIn(cloneObject));
            }
        }

        private bool CanSpawn(SwitchController switchController)
        {
            //name is quickfix for doors
            return switchController != null ? switchController.IsPressed(PrefabToSpawn.layer, this.name) : true;
        }

        private IEnumerator FadeIn(GameObject cloneObject)
        {
            float fadeTime = cloneObject.GetComponentInChildren<ParticleSystem>().main.duration * FadeTime;
            float elapsedTime = 0f;

            Color color = cloneObject.GetComponent<MeshRenderer>().material.color;
            while (elapsedTime < fadeTime && cloneObject != null)
            {
                elapsedTime += Time.deltaTime;
                cloneObject.GetComponent<MeshRenderer>().material.color = Color.Lerp(Color.clear, color, elapsedTime / fadeTime);
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

        private IEnumerator DestoyDelay(GameObject gameObject)
        {
            ParticleSystem ps = gameObject.GetComponentInChildren<ParticleSystem>();
            ParticleSystem.MainModule main = ps.main;
            GameObject psObject = ps.gameObject;
            float psDuration = main.duration;

            gameObject.transform.DetachChildren();
            gameObject.SetActive(false);
            main.prewarm = true;
            ps.Play();
            yield return new WaitUntil(() => ps.isStopped == true);
            Destroy(gameObject);
            Destroy(psObject);
        }

        private void OnDrawGizmos()
        {
            if (ShowGizmos)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawSphere(transform.position + SpawnLocationOffset, 0.5f);
            }
        }
    }
}