﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzlePlatformer
{
    public class UIManager : MonoBehaviour
    {
        public GameObject PanelF;
        public GameObject PanelE;

        //private EventManager _eventManager;
        private string _grab = "grab";
        private string _release = "release";
        private string _textInitialF;
        private string _textInitialE;
        private Text _textF;
        private Text _textE;

        private void Start()
        {
            //EventManager _eventManager = FindObjectOfType<EventManager>();
            //EventManager.InstructionGrab += ShowGrab;
            //EventManager.InstructionSpawn += ShowSpawn;
            EventManager.UserInterface += UIControl;

            _textF = PanelF.GetComponentInChildren<Text>();
            _textE = PanelE.GetComponentInChildren<Text>();

            _textInitialF = _textF.text;
            _textInitialE = _textE.text;
        }

        private void ShowGrab(bool shouldDisplay, bool objectGrabbed)
        {
            if (shouldDisplay && !PanelF.activeInHierarchy)
            {
                if (!objectGrabbed)
                {
                    _textF.text = _textInitialF + _grab;
                }
                else
                    _textF.text = _textInitialF + _release;

                PanelF.SetActive(true);
            }

            else if (!shouldDisplay)
            {
                PanelF.SetActive(false);
            }
        }

        private void ShowSpawn(bool canSpawn, bool shouldShow)
        {
            _textE.text = _textInitialE;
            if (canSpawn)
            {
                _textE.text = _textInitialE;
                PanelE.SetActive(true);
            }

            else if (!canSpawn)
            {
                _textE.text = "Switch not pressed";
                PanelE.SetActive(true);
            }

            PanelE.SetActive(shouldShow);
        }

        private void ShowOpen(bool locked, bool opened)
        {
            if (locked)
            {
                _textE.text = "Locked";
            }
            else
            {
                if (opened)
                {
                    _textE.text = "Press E to close";
                }
                else
                {
                    _textE.text = "Press E to open";
                }
            }

            PanelE.SetActive(true);
        }

        private void UIControl(GameObject gameObject, bool state)
        {
            switch (gameObject.layer)
            {
                case 11:
                    ObjectSpawnerButton button = gameObject.GetComponent<ObjectSpawnerButton>();
                    if (button != null)
                    {
                        if (button.Switch != null)
                        {
                            ShowSpawn(button.Switch.IsPressed(button.PrefabToSpawn.layer, this.name), true);
                        }
                        else
                        {
                            ShowSpawn(true, true);
                        }
                    }
                    break;

                case 12:
                case 13:
                    ShowGrab(true, state);
                    break;
                case 14:
                    ShowOpen(!gameObject.GetComponent<DoorController>().DoorLocked, gameObject.GetComponent<DoorController>().DoorOpened);
                    break;
                default:
                    ShowSpawn(false, false);
                    ShowGrab(false, state);
                    PanelE.SetActive(false);
                    break;
            }
        }
    }
}
